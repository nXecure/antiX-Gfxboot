# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Ivica  Kolić <ikoli@yahoo.com>, 2021
# James Bowlin <BitJam@gmail.com>, 2021
# anticapitalista <anticapitalista@riseup.net>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-04 21:24+0200\n"
"PO-Revision-Date: 2021-07-09 16:40+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021\n"
"Language-Team: Croatian (https://www.transifex.com/anticapitalista/teams/10162/hr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hr\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

msgid "Advanced Options"
msgstr ""

msgid "Back to main menu"
msgstr ""

msgid "Back to main menu (or press »ESC«)"
msgstr ""

msgid "Boot Process Performance Visualization"
msgstr ""

msgid "Boot Rescue Menus"
msgstr ""

msgid "Boot options"
msgstr ""

msgid "Change passwords before booting"
msgstr ""

msgid "Check LiveUSB and persistence ext2/3/4 file systems"
msgstr ""

msgid "Check integrity of the live media"
msgstr ""

msgid "Console options"
msgstr ""

msgid "Copy the compressed file system to RAM"
msgstr "Kopiraj komprimirani datotečni sustav u RAM"

msgid "Custom"
msgstr ""

msgid "Customize Boot (text menus)"
msgstr ""

msgid "Desktop options"
msgstr ""

msgid "Disable ACPI"
msgstr ""

msgid "Disable GRUB theme"
msgstr ""

msgid "Disable Intel graphics invert"
msgstr ""

msgid "Disable LiveUSB-Storage feature"
msgstr ""

msgid "Disable automount via fstab"
msgstr ""

msgid "Disable console width"
msgstr ""

msgid "Disable dual video card detection"
msgstr ""

msgid "Disable swap"
msgstr ""

msgid "Disable text-splash screen"
msgstr ""

msgid "Don't look for USB-2 devices"
msgstr ""

msgid "Don't save files across reboots"
msgstr ""

msgid "Don't set repositories based on timezone."
msgstr ""

msgid "EFI Bootloader"
msgstr ""

msgid "Enable EFI subsystem for RT-PREEMPT kernels"
msgstr ""

msgid "Enable GRUB theme"
msgstr ""

msgid "Enable LiveUSB-Storage feature"
msgstr ""

msgid "Enable automount via fstab"
msgstr ""

msgid "Enable dual video card detection"
msgstr ""

msgid "Enable text-splash screen"
msgstr ""

msgid "Failsafe options"
msgstr ""

msgid "Finish booting from a LiveUSB"
msgstr ""

msgid "Finish booting from a LiveUSB or hard drive"
msgstr ""

msgid "Finish booting from a hard drive"
msgstr ""

msgid "Frugal Menus"
msgstr ""

msgid "Frugal like p_static_root"
msgstr ""

msgid "Frugal like persist_all"
msgstr ""

msgid "Frugal like persist_home"
msgstr ""

msgid "Frugal like persist_root"
msgstr ""

msgid "Frugal like persist_static"
msgstr ""

msgid "Frugal menu"
msgstr ""

msgid "Frugal menus"
msgstr ""

msgid "GFX menu and GRUB menu"
msgstr ""

msgid "GRUB Bootloader"
msgstr ""

msgid "GRUB Menus"
msgstr ""

msgid "GRUB bootloader"
msgstr ""

msgid "GRUB loader"
msgstr ""

msgid "GRUB menu"
msgstr ""

msgid "GRUB menus"
msgstr ""

msgid "GRUB theme"
msgstr ""

msgid "GRUB-EFI bootloader"
msgstr ""

msgid "Hardware clock uses UTC (Linux)"
msgstr ""

msgid "Hardware clock uses local time (Windows)"
msgstr ""

msgid "Help"
msgstr "Pomoć"

msgid "Invert video on some Intel graphics systems"
msgstr ""

msgid "Kernel options"
msgstr ""

msgid "Keyboard"
msgstr ""

msgid "Language"
msgstr "Jezik"

msgid "Memory Test"
msgstr "Test memorije"

msgid "No EFI bootloader found."
msgstr ""

msgid "No Frugal menu found."
msgstr ""

msgid "No GRUB CFG-Menu found."
msgstr ""

msgid "No GRUB bootloader found."
msgstr ""

msgid "No Shim-EFI bootloader found."
msgstr ""

msgid "No Windows bootloader found."
msgstr ""

msgid "Only Frugal, no persistence"
msgstr ""

msgid "Only »/home« on persistence device"
msgstr ""

msgid "Persistence option"
msgstr ""

msgid "Power Off"
msgstr "Isključi napajanje"

msgid "Press <Enter> to continue"
msgstr ""

msgid "Press »Enter« to continue"
msgstr ""

msgid "Press »e« to edit, »ESC« to go back."
msgstr ""

msgid "Reboot"
msgstr "Restartování"

msgid "Reboot into BIOS/UEFI Setup"
msgstr ""

msgid "Reset"
msgstr ""

msgid "Save options"
msgstr ""

msgid "Save options (LiveUSB only)"
msgstr ""

msgid "Save some files across reboots"
msgstr ""

msgid "Secure Boot Restrictions"
msgstr ""

msgid "Shim-EFI bootloader"
msgstr ""

msgid "Show text menus"
msgstr ""

msgid "Show video card detection menu"
msgstr ""

msgid "Switch to Syslinux"
msgstr ""

msgid "The highlighted entry will start in %d seconds."
msgstr ""

msgid "Timezone"
msgstr ""

msgid "UTC or local time is asked"
msgstr ""

msgid "Use ↑ and ↓. Hit »ENTER« to select/deselect."
msgstr ""

msgid "Windows Bootloader"
msgstr ""

msgid "Windows bootloader"
msgstr ""

msgid "Windows bootloader on drive"
msgstr ""

msgid "disabled"
msgstr "onemogućeno"

msgid "enabled"
msgstr "omogućeno"

msgid "finished"
msgstr ""

msgid "found EFI bootloader at"
msgstr ""

msgid "found Frugal menu at"
msgstr ""

msgid "found GRUB bootloader at"
msgstr ""

msgid "found GRUB menu on"
msgstr ""

msgid "found Windows bootloader on drive"
msgstr ""

msgid "root »/« and »/home« in RAM"
msgstr ""

msgid "root »/« and »/home« separate on persistence device"
msgstr ""

msgid "root »/« and »/home« together on persistence device"
msgstr ""

msgid "root »/« in RAM, »/home« on persistence device"
msgstr ""

msgid "searching for Frugal grub.entry menus"
msgstr ""

msgid "searching for GRUB bootloader"
msgstr ""

msgid "searching for GRUB menus"
msgstr ""

msgid "searching for GRUB-EFI bootloader"
msgstr ""

msgid "searching for Shim-EFI bootloader"
msgstr ""

msgid "searching for Windows bootloader"
msgstr ""

msgid "version"
msgstr ""

msgid "Disable remastering even if linuxfs.new is found."
msgstr ""

msgid "Roll back to the previous remastered version."
msgstr ""

msgid "Show frugal device selection - can be saved"
msgstr ""

msgid "Show frugal device selection - one time, not saved"
msgstr ""

msgid "found Windows bootloader on"
msgstr ""

msgid ""
"Minimal BASH-like line editing is supported. For the first word, TAB lists "
"possible command completions. Anywhere else TAB lists possible device or "
"file completions. %s"
msgstr ""
"Uređivanje u načinu rada minimalnog BASH-a podržano. Za prvu riječ, TAB "
"ispisuje dostupne naredbe. Bilo gdje drugdje TAB ispisiva dostupne uređaje "
"ili dostupne datoteke. %s"

msgid ""
"Minimum Emacs-like screen editing is supported. TAB lists completions. Press"
" Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard "
"edits and return to the GRUB menu."
msgstr ""
"Uređivanje u načinu rada minimalnog Emacsa podržano. TAB ispis dostupnih "
"naredba. Pritisnite Ctrl-x ili F10 za pokretanje, Ctrl-c ili F2 za naredbeni"
" redak ESC za odbacivanje promjena i povratak u GRUB izbornik."
