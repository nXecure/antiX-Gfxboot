#!/bin/bash

# usage
[ x${1} != x${1#-} ] && cat<<'USAGE' && exit

grub-mo.sh - script to generate grub compiled translation mo-files

   usage: grub-mo.sh [lang1 lang2 ... ]

   without lang parameter all po-files within ./tx directory are processed
   
USAGE

# ignore transifex po-translation for translation completion less xx%
TX_COMPLETION_LEVEL=30

# put lang parameter into hash, if any
unset lang
[ -n "$*" ] && eval declare -A lang=( $(printf '["%s"]="x" ' "$@") )

# generated grub-mo-files will replace all mo-files within Input dir 
# at ../../Input/common/iso/boot/grub/config/locale
GRUB_LOCALE_DIR=../../Input/common/iso/boot/grub/config/locale

# some globale definitions
# grub built-in strings used with grub theme
unset KEYMAPS
readarray -t KEYMAPS <<'EOL'
Press enter to boot the selected OS, `e' to edit the commands before booting or `c' for a command-line.
The highlighted entry will be executed automatically in %ds.
enter: boot, `e': options, `c': cmd-line
Use the %C and %C keys to select which entry is highlighted.
Press enter to boot the selected OS, `e' to edit the commands before booting or `c' for a command-line. ESC to return previous menu.
EOL

# not used
readarray -t KEYMAPS_NEW_not_used <<'EOL'
Use the ↑ and ↓ keys to highlight an entry. Hit »ENTER« to select/deselect.
The highlighted entry will start in %d seconds.
Press 'e' to edit the selected entry, press »ESC« to go back.
Use ↑ and ↓. Hit »ENTER« to select/deselect.
Press »e« to edit, »ESC« to go back.
EOL

# using re-mapped English keymaps
unset KEYMAPS_NEW
readarray -t KEYMAPS_NEW <<'EOL'
Use ↑ and ↓. Hit »ENTER« to select/deselect.
The highlighted entry will start in %d seconds.
Press »e« to edit, »ESC« to go back.
Use ↑ and ↓. Hit »ENTER« to select/deselect.
Press »e« to edit, »ESC« to go back.
EOL


[ -d $GRUB_LOCALE_DIR ] || {
    echo "directory '$GRUB_LOCALE_DIR' not found .. exit"
    exit
}
[ -d tx ] || {

    echo "directory 'tx' not found  or not populated wit po-files .. exit"
    echo "to get all po files use: tx pull  --source --force --all"
    exit
}

# check we have transifex po files to process
[ $(ls -1 tx/*.po|wc -l) = 0 ] && {
    echo "No po-files in tx-folder. exit..."
    exit 1
    }

# check en.pot source file exists
[ -f en.pot ] || {
    echo "No source en.pot found .. exit"
    exit
}
COUNT_POT_MSGIDS=$(grep -c ^msgid en.pot)

# clear all mo files in GRUB_LOCALE_DIR if no lang-paramter is given
[ ${#lang[@]} == 0 ] && rm $GRUB_LOCALE_DIR/*.mo 2>/dev/null

for TX_PO in $(ls tx/*.po 2>/dev/null); do
	TX_LG=${TX_PO%.po}; TX_LG=${TX_LG##*/}
 	
	[ ${#lang[@]} == 0 ] ||  [ -n  "${lang[$TX_LG]}" ] || continue

    TX_MO=/tmp/${TX_LG}.mo
    TX_PO_REPACK=${TX_PO}_repack

    # trans_table will hold found keymap translations
    # pre-populate trans_table with English terms
    unset trans_table
    declare -A trans_table
    for x in "${KEYMAPS_NEW[@]}"; do
        trans_table[$x]=$x
    done

    # populate keymap_table with English terms
    unset keymap_table
    declare -A keymap_table
    #for i in {0..2}; do
    for i in $(seq 0 $((${#KEYMAPS[@]}-1))); do
        k=${KEYMAPS[$i]}
        t=${KEYMAPS_NEW[$i]}
        keymap_table[$k]=$t
    done

    # re-packed po-file to get rid of empty tramslations
    # compile po to mo - will remove and empty msgstr translations
    msgfmt -o ${TX_MO}  ${TX_PO}
    # uncompile again - will have no untranslated msgid/msgstr
    msgunfmt --no-wrap  --sort-output -o ${TX_PO_REPACK} ${TX_MO}
    rm ${TX_MO}

    [ -f ${TX_PO_REPACK} ] || continue

    COUNT_PO_MSGIDS=$(grep -c ^msgid $TX_PO_REPACK)
    # ignore po-translations if less then 80% translated
    if (( (COUNT_PO_MSGIDS * 100)/COUNT_POT_MSGIDS < TX_COMPLETION_LEVEL )); then
        rm  ${TX_PO_REPACK}
       continue
    fi
    sed -i 's:PACKAGE VERSION:antix-development:'  ${TX_PO_REPACK}

    # fix trailing white spaced with msgid and msgstr
    # trailing white spaces have been fixed in source, so will remove here
    sed -i -r 's/[[:space:]]+"$/"/' ${TX_PO_REPACK}

    # extract translations from po-file, if any
    for T in "${!trans_table[@]}"; do
        #grep -A1 "$T" $TX_PO_REPACK | grep -oP 'msgstr "\K.*' | sed 's/"$//'
        XT=$(grep -A1 "$T" $TX_PO_REPACK | grep -oP 'msgstr "\K.*' | sed 's/"$//')
        : ${XT:=$T}
        trans_table[$T]=$XT
    done

    # translate keymap table
    for K in "${!keymap_table[@]}"; do
        T=${keymap_table["$K"]}
        XT=${trans_table["$T"]}
        keymap_table["$K"]=$XT
    done

    # append translations to po file
    for K in "${!keymap_table[@]}"; do
        T=${keymap_table[$K]}
        printf '\nmsgid "%s"\n' "$K"   >> $TX_PO_REPACK
        printf   'msgstr "%s"\n' "$T"  >> $TX_PO_REPACK
    done

    # generate mo-file for grub to use
    TX_MO=${TX_MO##*/}
    TX_MO=$GRUB_LOCALE_DIR/${TX_MO}
    echo msgfmt -o $TX_MO  $TX_PO_REPACK
    msgfmt -o $TX_MO  $TX_PO_REPACK
    rm $TX_PO_REPACK
done

# check to generate en.mo (theme string "translations")
[ ${#lang[@]} == 0 ] ||  [ -n  "${lang[en]}" ] || exit

# generate theme translations file en.mo
EN_PO=/tmp/en-en.po
[ -f $EN_PO ] && rm $EN_PO

cat <<'EOL' > $EN_PO
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
EOL

# append translations to po file
for i in $(seq 0 $((${#KEYMAPS[@]}-1))); do
    K=${KEYMAPS[$i]}
    T=${KEYMAPS_NEW[$i]}
    printf '\nmsgid "%s"\n' "$K"   >> $EN_PO
    printf   'msgstr "%s"\n' "$T"  >> $EN_PO
done
EN_MO=$GRUB_LOCALE_DIR/en.mo
[ -f $EN_MO ] && rm $EN_MO
echo msgfmt -o $EN_MO  $EN_PO
msgfmt -o $EN_MO  $EN_PO
#rm $EN_PO
