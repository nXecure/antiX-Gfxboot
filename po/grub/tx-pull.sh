#!/bin/bash

# usage
[ x${1} != x${1#-} ] && cat<<'USAGE' && exit

tx-pull.sh - fetch po-files from transifex 
             for antix-development.live-grubpot
             but not en.pot source file

Usage: tx-pull.sh [lang1 lang2 ... ]

   without lang-parameter all available languages are fetched.
   en.pot source will not be downloaded.

Note: If TX_TOKEN is unset, it will ask to enter (will not be saved).
      Leave empty (press enter) to get transifex authentication prompt.

USAGE

# create transifex .tx/config
[ -d .tx ] || mkdir .tx

cat <<EOF > .tx/config
[main]
host = https://www.transifex.com

[antix-development.live-grubpot]
file_filter = tx/<lang>.po
minimum_perc = 0
source_file = en.pot
source_lang = en
type = PO
EOF

# transifex authentication check

if ! grep -sq password  ~/.transifexrc; then
    if [[ -z ${TX_TOKEN}  ]]; then
        echo "Please enter api token here, will not be saved to ~/.transifexrc."
        echo "Leave empty [press enter] to get it saved when asked again"
        read -r -s -p "Enter your api token: " TX_TOKEN
        export TX_TOKEN
        [[ -z ${TX_TOKEN}  ]] && unset TX_TOKEN
        echo
    fi
fi

lang="$*"
lang=${lang:+ --language ${lang// /,}}

#echo tx pull  --source --force ${lang:- --all}
#tx pull  --source --force ${lang:- --all }

echo tx pull  --force ${lang:- --all}
tx pull  --force ${lang:- --all }

