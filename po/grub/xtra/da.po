msgid "Minimal BASH-like line editing is supported. For the first word, TAB lists possible command completions. Anywhere else TAB lists possible device or file completions. %s"
msgstr "Minimal BASH-agtig linjeredigering understøttes. For det første ord vil TAB vise de mulige kommandofuldførelser. Alle andre steder vil TAB vise de mulige fuldførelser af enheds- eller filnavne. %s"

msgid "Minimum Emacs-like screen editing is supported. TAB lists completions. Press Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard edits and return to the GRUB menu."
msgstr "Der understøttes minimal Emacs-agtig skærmredigering. TAB viser fuldførelser. Tryk på Ctrl-x eller F10 for at begynde opstart, Ctrl-c eller F2 for at få en kommandolinje, eller ESC for at forkaste ændringerne og vende tilbage til GRUB-menuen."
